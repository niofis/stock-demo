**Requirements**
- Visual Studio 2015
- Windows 10
- Wt v3.3.6 for msvs2015, x86. Wich can be downloaded from https://github.com/emweb/wt/releases
- Casablanca C++ REST SDK v2.9.1. Installed through NuGet.
- SQL Server 2014 Express Edition
- MS ADO Libraries

**Configuration**
Before proceding to build the application, VS2015 must be configured to find the required libraries.

For adding Wt to Stock-Client:
1. Right-click the solution.
2. Select Properties.
3. Follow the tree to the key *Configuration Properties -> C/C++ -> General -> Aditional Include Directories*
4. Enter the path to where the Wt SDK *include* folder is located, for example: *C:\Enrique\Code\Wt 3.3.6 msvs2015 x86\include*
5. Back track to the following key: *Configuration Properties -> Linker -> General -> Additional Library Directoreis*
6. Enter the path to where the Wt SDK *lib* folder is located, for example: *C:\Enrique\Code\Wt 3.3.6 msvs2015 x86\lib*
7. Finally find the next section: *Configuration Properties -> Linker -> Input -> Additional Dependencies*
8. Add the following libraries for DEBUG: *wtd.lib* and *wthttpd.lib*; or *wt.lib* and *wthttp.lib* if you are building for RELEASE.
9. Click OK, and you are done.

For Stock-Server it is required to especify the location of the msado15.dll:
1. Open the Sock-Server project
2. Find the *Database.h* file, located in the *Header Files* section.
3. Line 3 of the file includes the #import directive with the location of the dll.
4. Change the path to the correct location, for example: *"C:\Program Files\Common Files\System\ado\msado15.dll"*


**Database (for Stock-Server)**
This application requires SQL Server 2014 installation and a dedicated database for saving and retrieving data. 
After installing SQL Server, you must run the script located in *Database/db_create.sql*

You also need to create a user and grant it read and write permissions for the *Stock* database.
This credentials must be entered into the *connection_string.txt* file located in the project folder.

If you like, you can use the script *Database/db_example_data.sql* to insert some test data into the database.

Example connection string:
Provider = 'sqloledb'; Data Source = '(local)'; Initial Catalog = 'Stock';User Id='sa';Password='123456'

**Building**
Simply select *Build -> Build Solution* from the Visual Studio 2015 menu. Make sure the platform is set to x86 unless 
you have the required librearies for other platforms.

**Running**

Running Stock-Client requires configuring the base address for the API url, which can be specified in the file *api_url.txt* 
found in the project directory. This file needs to be in the same folder as the Stock-Client.exe if it's gonna be executed outside VS2015.

The default URL is the following:
http://127.0.0.1:8080

Notice there is no trailing '/'.

There are some files that must be located in the same directory as the executables, in order for them to run.

Stock-Server requires:
DEBUG:
- cpprest140d_2_9.dll

RELEASE:
- cpprest140_2_9.dll

Stock-Client requires:
DEBUG:
- cpprest140d_2_9.dll
- wtd.dll
- wthttpd.dll
- libeay32d.dll
- ssleay32d.dll

RELEASE:
- cpprest140_2_9.dll
- wt.dll
- wthttp.dll
- libeay32.dll
- ssleay32.dll


Running Stock-Client standalone (not from within VS2015), also requires the following command line parameters:
Stock-Client.exe --http-address=0.0.0.0 --http-port=8081 --deploy-path=/ --docroot=.
and copying the folder *resources*, found inside the project directory.

**Tests**

Unit tests are included in the Visual Studio project and can be run using th *Text Explorer* tool.
Before running the tests, make sure that the Stock-Server is running and its connection string is propertly configured in its corresponding config file.
