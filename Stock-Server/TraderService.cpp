#include "stdafx.h"
#include "TraderService.h"
#include "Utils.h"
#include "Trader.h"

using namespace std;
using namespace utility;
using namespace web;


void TraderService::get(http_request message)
{
  //Make it lowercase
  //from: http://stackoverflow.com/a/313990
  auto str_uri = message.request_uri().to_string();
  transform(str_uri.begin(), str_uri.end(), str_uri.begin(), ::tolower);
  vector<string_t> path = uri::split_path(str_uri);

  if (path.size() < 2) {
    message.reply(status_codes::NotFound);
    return;
  }

  auto action = path[1];

  ucout << str_uri << endl;

  message.reply(status_codes::OK);
}

void TraderService::post(http_request message)
{
  //Make it lowercase
  //from: http://stackoverflow.com/a/313990
  auto str_uri = message.request_uri().to_string();
  transform(str_uri.begin(), str_uri.end(), str_uri.begin(), ::tolower);
  vector<string_t> path = uri::split_path(str_uri);

  if (path.size() < 2) {
    message.reply(status_codes::NotFound);
    return;
  }

  auto action = path[1];
  auto json = message.extract_json().get();

  if (json.is_null()) {
    json::value response;
    response[U("error")] = json::value::string(U("incomplete credentials."));
    message.reply(status_codes::BadRequest, response);
    return;
  }

  auto j_username = json[U("username")];
  auto j_password = json[U("password")];

  if (j_username.is_null() || j_password.is_null()) {
    json::value response;
    response[U("error")] = json::value::string(U("incomplete credentials."));
    message.reply(status_codes::BadRequest, response);
    return;
  }

  auto t_username = j_username.as_string();
  auto t_password = j_password.as_string();

  auto user = utility::conversions::to_utf8string(t_username);
  auto pwd = utility::conversions::to_utf8string(t_password);


  json::value response;

  Trader trader(user, pwd);

  if (action.compare(U("register")) == 0) {
    auto res = trader.Register();

    response = Utils::ToJSON(res);

    if (res.find("error") != res.end()) {
      message.reply(status_codes::BadRequest, response);
    }
    else {
      message.reply(status_codes::OK, response);
    }

    return;
  }


  trader.Login();

  if (trader.isLoggedIn == false) {
    response[U("error")] = json::value::string(U("invalid credentials."));
    message.reply(status_codes::Forbidden, response);
    return;
  }
  
  if (action.compare(U("login")) == 0) {
    //Already logged in, simply return ok
      response[U("result")] = json::value::string(U("ok"));
      message.reply(status_codes::OK, response);
  }
  else if (action.compare(U("transactions")) == 0) {
      auto log = trader.GetTransactions();
      response = Utils::ToJSON(log);
      message.reply(status_codes::OK, response);

  }
  else if (action.compare(U("portfolio")) == 0) {
      auto portfolio = trader.GetPortfolio();
      response = Utils::ToJSON(portfolio);
      message.reply(status_codes::OK, response);

  }
  else if (action.compare(U("balance")) == 0) {
    double balance = trader.GetBalance();
    response[U("balance")] = json::value::number(balance);
    message.reply(status_codes::OK, response);
  }
  else if (action.compare(U("delete")) == 0) {
    auto res = trader.Delete();
    if (trader.Delete() == false) {
      response[U("error")] = json::value::string(U("couldn't delete account."));
      message.reply(status_codes::BadRequest, response);
    }
    else {
      response[U("result")] = json::value::string(U("ok"));
      message.reply(status_codes::OK, response);
    }
    
  }
}