#include "stdafx.h"
#include "Transaction.h"

using namespace std;
using namespace web;
using namespace utility::conversions;
Transaction::Transaction()
{
  id = "";
  username = "";
  stockcode = "";
  quantity = 0;
  datetime = "";
  status = "";
  type = "";
}

Transaction::Transaction(map<string, string> data)
  : Transaction()
{
  if (data.find("id") != data.end())
    id = data["id"];

  if (data.find("username") != data.end())
    username = data["username"];

  if (data.find("stockcode") != data.end())
    stockcode = data["stockcode"];

  if (data.find("quantity") != data.end())
    quantity = stod(data["quantity"]);

  if (data.find("datetime") != data.end())
    datetime = data["datetime"];

  if (data.find("status") != data.end())
    status = data["status"];

  if (data.find("type") != data.end())
    type = data["type"];
}

bool Transaction::Create()
{
  return Database::GetInstance().InsertTransaction(username, stockcode, quantity, status, type);
}

json::value Transaction::ToJSON()
{
  json::value result;

  result[U("id")] = json::value::string(to_string_t(id));
  result[U("username")] = json::value::string(to_string_t(username));
  result[U("stockcode")] = json::value::string(to_string_t(stockcode));
  result[U("quantity")] = json::value::number(quantity);
  result[U("datetime")] = json::value::string(to_string_t(datetime));
  result[U("status")] = json::value::string(to_string_t(status));
  result[U("type")] = json::value::string(to_string_t(type));

  return result;
}
