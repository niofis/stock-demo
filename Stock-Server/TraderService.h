#pragma once
#include "stdafx.h"
#include "Controller.h"

using namespace web;
using namespace http;

class TraderService : public Controller
{
public:
  TraderService(utility::string_t url) : Controller(url) {};

private:

  void get(http_request message);
  void post(http_request message);
};