#pragma once

#include "stdafx.h"

#include "Entity.h"
#include "Transaction.h"
#include "Portfolio.h"


using namespace std;
using namespace web;
using namespace utility;

class Utils
{
public:
  static web::json::value ToJSON(map<string, double>& data) {
    json::value result;
    for (auto e = data.begin(); e != data.end(); e++) {
      string_t f = utility::conversions::to_string_t(e->first);
      json::value s = json::value::number(e->second);
      result[f] = s;
    }
    return result;
  }

  static web::json::value ToJSON(map<string, string>& data) {
    json::value result;
    for (auto e = data.begin(); e != data.end(); e++) {
      string_t f = utility::conversions::to_string_t(e->first);
      json::value s = json::value::string(utility::conversions::to_string_t(e->second));
      result[f] = s;
    }
    return result;
  }

  static web::json::value ToJSON(vector<map<string, string>>& data) {
    auto result = json::value::array(data.size());
    for (int i = 0; i < data.size(); i++) {
      result[i] = Utils::ToJSON(data[i]);
    }
    return result;
  }

  static web::json::value ToJSON(vector<Transaction>& data) {
    auto result = json::value::array(data.size());
    for (int i = 0; i < data.size(); i++) {
      result[i] = data[i].ToJSON();
    }
    return result;
  }

  static web::json::value ToJSON(vector<Portfolio>& data) {
    auto result = json::value::array(data.size());
    for (int i = 0; i < data.size(); i++) {
      result[i] = data[i].ToJSON();
    }
    return result;
  }
};