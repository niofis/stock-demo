#include "stdafx.h"
#include "Trader.h"
#include "Portfolio.h"
#include "Transaction.h"

using namespace std;
using namespace web;
using namespace utility::conversions;

Portfolio::Portfolio()
{
  id = "";
  username = "";
  stockcode = "";
  quantity = 0;
  totalcost = 0;
}

Portfolio::Portfolio(map<string, string> data)
  : Portfolio()
{
  if (data.find("id") != data.end())
    id = data["id"];

  if (data.find("username") != data.end())
    username = data["username"];

  if (data.find("stockcode") != data.end())
    stockcode = data["stockcode"];

  if (data.find("quantity") != data.end())
    quantity = stod(data["quantity"]);

  if (data.find("totalcost") != data.end())
    totalcost = stod(data["totalcost"]);
}

bool Portfolio::Create()
{
  if (Database::GetInstance().InsertPortfolio(
    username, stockcode, quantity, totalcost) == false) {
    return false;
  }

  return true;
}

bool Portfolio::Update()
{
  if (Database::GetInstance().UpdatePortfolio(
    username,
    stockcode, quantity, totalcost) == false) {
    return false;
  }
  return true;
}

map<string, string> Portfolio::Sell(double quantity_to_sell, double sell_price)
{
  map<string, string> result;

  if (quantity_to_sell <= 0 || sell_price <= 0) {
    result["error"] = "quantity and price must be greater than zero.";
    return result;
  }

  if (this->quantity < quantity_to_sell) {
    result["error"] = "can't sell more stock than owned.";
    return result;
  }

  this->quantity -= quantity_to_sell;


  double delta = quantity_to_sell * sell_price;
  if (Database::GetInstance().AdjustBalance(username, delta) == false) {
    result["error"] = "internal error";
    return result;
  }

  double current_price = Database::GetInstance().StockPrice(stockcode);
  this->totalcost = this->quantity * current_price;

  if (Update() == false) {
    result["error"] = "internal error";
    return result;
  }

  Transaction tr;
  tr.username = username;
  tr.stockcode = stockcode;
  tr.quantity = quantity_to_sell;
  tr.status = "EXECUTED";
  tr.type = "SELL";

  if (tr.Create() == false) {
    result["error"] = "internal error";
    return result;
  }

  result["result"] = "ok";
  return result;
}

map<string, string> Portfolio::Buy(string user, string code, double quantity_to_buy, double buy_price)
{
  map<string, string> result;


  if (quantity_to_buy <= 0 || buy_price <= 0) {
    result["error"] = "quantity and price must be greater than zero.";
    return result;
  }

  auto quote = Database::GetInstance().Quote(code);
  if(quote.find("lastsaleprice") == quote.end()) {
    result["error"] = "stock code not valid.";
    return result;
  }

  Trader trader(user,"");
  Portfolio owned = trader.GetPortfolio(code);

  owned.username = trader.username;
  owned.stockcode = code;
  owned.quantity += quantity_to_buy;

  double delta = -quantity_to_buy * buy_price;

  double current_balance = trader.GetBalance();
  if (current_balance + delta < 0) {
    result["error"] = "don't have enough money.";
    return result;
  }

  if (Database::GetInstance().AdjustBalance(trader.username, delta) == false) {
    result["error"] = "internal error";
    return result;
  }

  owned.totalcost = owned.quantity * quote["lastsaleprice"];

  if (owned.id == "") {
    //First time buying this stock
    if (owned.Create() == false) {
      result["error"] = "internal error";
      return result;
    }
  }
  else {
    if (owned.Update() == false) {
      result["error"] = "internal error";
      return result;
    }
  }

  Transaction tr;
  tr.username = trader.username;
  tr.stockcode = owned.stockcode;
  tr.quantity = quantity_to_buy;
  tr.status = "EXECUTED";
  tr.type = "BUY";

  if (tr.Create() == false) {
    result["error"] = "internal error";
    return result;
  }

  result["result"] = "ok";
  return result;
}


json::value Portfolio::ToJSON()
{
  json::value result;

  result[U("id")] = json::value::string(to_string_t(id));
  result[U("username")] = json::value::string(to_string_t(username));
  result[U("stockcode")] = json::value::string(to_string_t(stockcode));
  result[U("quantity")] = json::value::number(quantity);
  result[U("totalcost")] = json::value::number(totalcost);

  return result;
}
