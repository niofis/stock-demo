#pragma once
#include "stdafx.h"

using namespace web;
using namespace http;

class Controller
{
public:

  Controller(utility::string_t url) : m_listener(url)
  {
    m_listener.support(methods::GET, bind(&Controller::get, this, std::placeholders::_1));
    m_listener.support(methods::POST, bind(&Controller::post, this, std::placeholders::_1));
  }

  pplx::task<void> open() { return m_listener.open(); }
  pplx::task<void> close() { return m_listener.close(); }

private:

  virtual void get(http_request message) = 0;
  virtual void post(http_request message) = 0;


  experimental::listener::http_listener m_listener;
};