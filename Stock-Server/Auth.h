#pragma once
#include "stdafx.h"
#include "Database.h"
#include "Trader.h"

using namespace std;
class Auth
{
public:
  static Trader Login(string username, string password);
  static bool IsValid(string username, string password) 
  {
    return false;
  }
  static bool IsValid(web::json::value json)
  {
    //TODO: HASH PASSWORD!
    auto t_username = json[U("username")].as_string();
    auto t_password = json[U("password")].as_string();

    if (t_username.size() == 0 || t_password.size() == 0)
      return false;

    auto user = Database::GetInstance().FindTrader(utility::conversions::to_utf8string(t_username));
    auto pwd = utility::conversions::to_utf8string(t_password);

    if (user["password"] == pwd)
      return true;

    return false;
  }
};