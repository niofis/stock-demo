#pragma once
#include "stdafx.h"
#include "Database.h"
#include "Transaction.h"
#include "Portfolio.h"

using namespace std;

class Trader
{
public:
  string username;
  string password;
  bool isLoggedIn;

  Trader(string username, string password);
  map<string, string> Register();
  bool Login();
  bool Delete();
  
  vector<Transaction> GetTransactions();
  vector<Portfolio> GetPortfolio();
  Portfolio GetPortfolio(string stockcode);
  double GetBalance();

};