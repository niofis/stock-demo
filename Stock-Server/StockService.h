#pragma once
#include "stdafx.h"
#include "Controller.h"

using namespace web;
using namespace http;

class StockService : public Controller
{
public:
  StockService(utility::string_t url) : Controller(url) {};

private:

  void get(http_request message);
  void post(http_request message);
};