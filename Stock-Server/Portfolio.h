#pragma once
#include "stdafx.h"
#include "Entity.h"
#include "Database.h"

using namespace std;

class Portfolio : public Entity
{
public:
  string id;
  string username;
  string stockcode;
  double quantity;
  double totalcost;

  Portfolio();
  Portfolio(map<string, string> data);

  bool Create();
  bool Update();
  map<string, string> Sell(double quantity, double price);

  map<string, string> Buy(string user, string code, double quantity_to_buy, double buy_price);

  json::value ToJSON();
};