#include "stdafx.h"
#include "Database.h"
#include <oledb.h>

//TODO: All db params must be sanitized

string Database::connectionString = "";

Database& Database::GetInstance()
{
  static Database instance;
  return instance;
}

void Database::SetConnectionString(string connection_string)
{
  connectionString = connection_string;
}

Database::~Database()
{
}

_ConnectionPtr Database::GetConnection()
{
  static _ConnectionPtr cnn;
  try {
    if (cnn)
      return cnn;
    _bstr_t strCnn(Database::connectionString.c_str());
    HRESULT hr = cnn.CreateInstance(__uuidof(Connection));
    cnn->Open(strCnn, "", "", adConnectUnspecified);
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }
  return cnn;
}

_RecordsetPtr Database::Execute(string stmt)
{

  return NULL;
}

map<string, string> Database::FindTrader(string username)
{
  map<string, string> trader;
  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select top 1 * from trader where username = '");
    query += username.c_str();
    query += "'";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
      string usr((LPCSTR)(_bstr_t)res->Fields->GetItem("username")->Value);
      trader["username"] = usr;

      string pwd((LPCSTR)(_bstr_t)res->Fields->GetItem("password")->Value);
      trader["password"] = pwd;

      string blc((LPCSTR)(_bstr_t)res->Fields->GetItem("balancecash")->Value);
      trader["balancecash"] = blc;
    }
    res->Close();
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }
  return trader;
}

map<string, string> Database::RegisterTrader(string username, string password)
{
  map<string, string> res;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Insert into trader(username, password, balancecash) values('");
    query += username.c_str();
    query += "','";
    query += password.c_str();
    query += "',100000)";
    conn->Execute(query, NULL, 1);

    res["result"] = "ok";

  }
  catch (_com_error &e)
  {
    res["error"] = "Unkown error.";
    cout << e.Description() << endl;
  }

  return res;
}

vector<map<string, string>> Database::Transactions(string username)
{
  vector<map<string, string>> results;

  try {

    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select * from [transaction] where username = '");
    query += username.c_str();
    query += "' order by datetime desc";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
    }
    while (res->EndOfFile == false) {
      map<string, string> transaction;

      string id((LPCSTR)(_bstr_t)res->Fields->GetItem("id")->Value);
      transaction["id"] = id;

      string usr((LPCSTR)(_bstr_t)res->Fields->GetItem("username")->Value);
      transaction["username"] = usr;

      string stockcode((LPCSTR)(_bstr_t)res->Fields->GetItem("stockcode")->Value);
      transaction["stockcode"] = stockcode;

      string quantity((LPCSTR)(_bstr_t)res->Fields->GetItem("quantity")->Value);
      transaction["quantity"] = quantity;

      string datetime((LPCSTR)(_bstr_t)res->Fields->GetItem("datetime")->Value);
      transaction["datetime"] = datetime;

      string status((LPCSTR)(_bstr_t)res->Fields->GetItem("status")->Value);
      transaction["status"] = status;

      string type((LPCSTR)(_bstr_t)res->Fields->GetItem("type")->Value);
      transaction["type"] = type;

      results.push_back(transaction);

      res->MoveNext();
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return results;
}

vector<map<string, string>> Database::Portfolio(string username)
{
  vector<map<string, string>> results;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select * from [portfolio] where username = '");
    query += username.c_str();
    query += "' order by stockcode";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
    }
    while (res->EndOfFile == false) {
      map<string, string> transaction;

      string id((LPCSTR)(_bstr_t)res->Fields->GetItem("id")->Value);
      transaction["id"] = id;

      string usr((LPCSTR)(_bstr_t)res->Fields->GetItem("username")->Value);
      transaction["username"] = usr;

      string stockcode((LPCSTR)(_bstr_t)res->Fields->GetItem("stockcode")->Value);
      transaction["stockcode"] = stockcode;

      string quantity((LPCSTR)(_bstr_t)res->Fields->GetItem("quantity")->Value);
      transaction["quantity"] = quantity;

      string totalcost((LPCSTR)(_bstr_t)res->Fields->GetItem("totalcost")->Value);
      transaction["totalcost"] = totalcost;

      results.push_back(transaction);

      res->MoveNext();
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return results;
}

map<string, string> Database::Portfolio(string username, string stockcode)
{
  map<string, string> results;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select TOP 1 * from [portfolio] where username = '");
    query += username.c_str();
    query += "' and stockcode = '";
    query += stockcode.c_str();
    query += "'";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
      map<string, string> transaction;

      string id((LPCSTR)(_bstr_t)res->Fields->GetItem("id")->Value);
      results["id"] = id;

      string usr((LPCSTR)(_bstr_t)res->Fields->GetItem("username")->Value);
      results["username"] = usr;

      string stockcode((LPCSTR)(_bstr_t)res->Fields->GetItem("stockcode")->Value);
      results["stockcode"] = stockcode;

      string quantity((LPCSTR)(_bstr_t)res->Fields->GetItem("quantity")->Value);
      results["quantity"] = quantity;

      string totalcost((LPCSTR)(_bstr_t)res->Fields->GetItem("totalcost")->Value);
      results["totalcost"] = totalcost;

    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return results;
}

map<string, double> Database::Quote(string stockcode)
{
  map<string, double> quote;
  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select top 1 * from stock where stockcode = '");
    query += stockcode.c_str();
    query += "'";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();

      double lastsaleprice = (double)res->Fields->GetItem("lastsaleprice")->Value;
      quote["lastsaleprice"] = lastsaleprice;
    }
    res->Close();
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }
  return quote;
}

map<string, double> Database::OwnedStock(string username, string stockcode)
{
  map<string, double> result;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select * from [portfolio] where username = '");
    query += username.c_str();
    query += "' and stockcode ='";
    query += stockcode.c_str();
    query += "' order by stockcode";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
      double quantity = (double)res->Fields->GetItem("quantity")->Value;
      result["quantity"] = quantity;

      double totalcost = (double)res->Fields->GetItem("totalcost")->Value;
      result["totalcost"] = totalcost;
    }
    else {
      result["quantity"] = 0;
      result["totalcost"] = 0;
      result["not_owned"] = 1;
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

double Database::StockPrice(string stockcode)
{
  double result = 0;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select * from [stock] where stockcode = '");
    query += stockcode.c_str();
    query += "'";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
      double lassaleprice = (double)res->Fields->GetItem("lastsaleprice")->Value;
      result = lassaleprice;
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

double Database::TraderBalance(string username)
{
  double result = 0;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select balancecash from [trader] where username = '");
    query += username.c_str();
    query += "'";
    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
      double balancecash = (double)res->Fields->GetItem("balancecash")->Value;
      result = balancecash;
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

bool Database::AdjustBalance(string username, double delta)
{
  bool result = false;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Update trader set balancecash = balancecash + ");

    std::ostringstream str_delta;
    str_delta << delta;

    query += str_delta.str().c_str();
    query += " where username = '";
    query += username.c_str();
    query += "'";

    conn->Execute(query, NULL, 1);
    result = true;
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

bool Database::InsertTransaction(string username, string stockcode, double quantity, string status, string type)
{
  bool result = false;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Insert into [transaction](username, stockcode, quantity, type, status) values('");

    query += username.c_str();
    query += "','";

    query += stockcode.c_str();
    query += "',";

    std::ostringstream str_quantity;
    str_quantity << quantity;

    query += str_quantity.str().c_str();
    query += ",'";

    query += type.c_str();
    query += "','";

    query += status.c_str();
    query += "')";

    conn->Execute(query, NULL, 1);
    result = true;
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

bool Database::UpdatePortfolio(string username, string stockcode, double quantity, double cost)
{
  bool result = false;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Update [portfolio] set quantity = ");


    std::ostringstream str_quantity;
    str_quantity << quantity;

    query += str_quantity.str().c_str();

    query += ", totalcost = ";
    std::ostringstream str_cost;
    str_cost << cost;
    query += str_cost.str().c_str();

    query += " where username= '";
    query += username.c_str();

    query += "' and stockcode = '";

    query += stockcode.c_str();
    query += "'";

    conn->Execute(query, NULL, 1);
    result = true;
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}

bool Database::InsertPortfolio(string username, string stockcode, double quantity, double cost)
{
  bool result = false;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Insert into portfolio(username, stockcode, quantity, totalcost) values('");

    query += username.c_str();
    query += "','";
    query += stockcode.c_str();
    query += "',";
    std::ostringstream str_quantity;
    str_quantity << quantity;
    query += str_quantity.str().c_str();
    query += ",";
    std::ostringstream str_cost;
    str_cost << cost;
    query += str_cost.str().c_str();

    query += ")";

    conn->Execute(query, NULL, 1);
    result = true;
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}


vector<map<string, string>> Database::QuoteList()
{
  vector<map<string, string>> results;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Select * from [stock] order by stockcode");

    _RecordsetPtr res = conn->Execute(query, NULL, 1);
    if (res->EndOfFile == false) {
      res->MoveFirst();
    }
    while (res->EndOfFile == false) {
      map<string, string> quote;

      string stockcode((LPCSTR)(_bstr_t)res->Fields->GetItem("stockcode")->Value);
      quote["stockcode"] = stockcode;

      string lastsaleprice((LPCSTR)(_bstr_t)res->Fields->GetItem("lastsaleprice")->Value);
      quote["lastsaleprice"] = lastsaleprice;


      results.push_back(quote);

      res->MoveNext();
    }
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return results;
}

bool Database::DeleteTrader(string username)
{
  bool result = false;

  try {
    auto conn = Database::GetInstance().GetConnection();
    _bstr_t query = U("Delete from [trader] where username ='");

    query += username.c_str();
    query += "'";

    conn->Execute(query, NULL, 1);
    result = true;
  }
  catch (_com_error &e)
  {
    cout << e.Description() << endl;
  }

  return result;
}
