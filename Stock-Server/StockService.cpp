#include "stdafx.h"
#include "Auth.h"
#include "StockService.h"
#include "Utils.h"
#include "Trader.h"
#include "Portfolio.h"

using namespace std;
using namespace utility;
using namespace web;



void StockService::get(http_request message)
{
  //Make it lowercase
  //from: http://stackoverflow.com/a/313990
  auto str_uri = message.request_uri().to_string();
  transform(str_uri.begin(), str_uri.end(), str_uri.begin(), ::tolower);
  vector<string_t> path = uri::split_path(str_uri);

  if (path.size() < 2) {
    message.reply(status_codes::NotFound);
    return;
  }

  auto action = path[1];

  ucout << str_uri << endl;

  message.reply(status_codes::OK);
}

void StockService::post(http_request message)
{
  //Make it lowercase
  //from: http://stackoverflow.com/a/313990
  auto str_uri = message.request_uri().to_string();
  transform(str_uri.begin(), str_uri.end(), str_uri.begin(), ::tolower);
  vector<string_t> path = uri::split_path(str_uri);

  if (path.size() < 2) {
    message.reply(status_codes::NotFound);
    return;
  }

  auto action = path[1];
  json::value response;

  auto json = message.extract_json().get();

  if (json.is_null()) {
    response[U("error")] = json::value::string(U("incomplete credentials."));
    message.reply(status_codes::BadRequest, response);
    return;
  }

  auto j_username = json[U("username")];
  auto j_password = json[U("password")];

  if (j_username.is_null() || j_password.is_null()) {
    response[U("error")] = json::value::string(U("incomplete credentials."));
    message.reply(status_codes::BadRequest, response);
    return;
  }

  auto t_username = j_username.as_string();
  auto t_password = j_password.as_string();

  auto user = utility::conversions::to_utf8string(t_username);
  auto pwd = utility::conversions::to_utf8string(t_password);

  Trader trader(user, pwd);
  
  trader.Login();

  if (trader.isLoggedIn == false) {
    response[U("error")] = json::value::string(U("invalid credentials."));
    message.reply(status_codes::Forbidden, response);
    return;
  }


  if (action.compare(U("list")) == 0) {
    auto list = Database::GetInstance().QuoteList();
    json::value res;

    for (int i = 0; i < list.size(); i++) {
      json::value val;
      val[U("stockcode")] = json::value::string(utility::conversions::to_string_t(list[i]["stockcode"]));
      val[U("lastsaleprice")] = json::value::number(stod(list[i]["lastsaleprice"]));
      res[i] = val;
    }

    message.reply(status_codes::OK, res);

    return;
  }
  //From here on, stockcode is needed

  //Check if stockcode is present
  auto j_stc = json[U("stockcode")];
  if (j_stc.is_null()) {
    response[U("error")] = json::value::string(U("stockcode not specified."));
    message.reply(status_codes::BadRequest, response);
    return;
  }
  auto t_stockcode = j_stc.as_string();
  auto stockcode = utility::conversions::to_utf8string(t_stockcode);


  if (action.compare(U("sell")) == 0) {

    auto j_qty = json[U("quantity")];
    auto j_prc = json[U("price")];

    if (j_qty.is_null() ||
      j_prc.is_null() ||
      j_qty.is_number() == false ||
      j_prc.is_number() == false) {
      response[U("error")] = json::value::string(U("quantity and/or price was not specified."));
      message.reply(status_codes::BadRequest, response);
      return;
    }

    auto quantity_to_sell = j_qty.as_double();
    auto sell_price = j_prc.as_double();

    //auto owned = Database::GetInstance().OwnedStock(user, stockcode);
    auto owned = trader.GetPortfolio(stockcode);
    auto res = owned.Sell(quantity_to_sell, sell_price);

    response = Utils::ToJSON(res);

    if (res.find("error") != res.end()) {
      message.reply(status_codes::BadRequest, response);
      return;
    }

    message.reply(status_codes::OK, response);
  }
  else if (action.compare(U("buy")) == 0) {
    auto j_qty = json[U("quantity")];
    auto j_prc = json[U("price")];

    if (j_qty.is_null() || 
        j_prc.is_null() || 
        j_qty.is_number() == false ||
      j_prc.is_number() == false) {
      response[U("error")] = json::value::string(U("quantity and/or price was not specified."));
      message.reply(status_codes::BadRequest, response);
      return;
    }

    auto quantity_to_buy = j_qty.as_double();
    auto buy_price = j_prc.as_double();

    Portfolio prf;
    auto res = prf.Buy(trader.username, stockcode, quantity_to_buy, buy_price);

    response = Utils::ToJSON(res);

    if (res.find("error") != res.end()) {
      message.reply(status_codes::BadRequest, response);
      return;
    }

    message.reply(status_codes::OK, response);
  }
  else if (action.compare(U("quote")) == 0) {
    auto res = Utils::ToJSON(Database::GetInstance().Quote(stockcode));
    
    if (res.is_null()) {
      response[U("error")] = json::value::string(U("stockcode not found"));
      message.reply(status_codes::OK, response);
      return;
    }

    message.reply(status_codes::OK, res);
  }

}