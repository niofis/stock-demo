#pragma once
#include "stdafx.h"
#import "C:\Program Files\Common Files\System\ado\msado15.dll" no_namespace rename("EOF", "EndOfFile")



//based on code from:
//https://msdn.microsoft.com/en-us/library/ms676688(v=vs.85).aspx
//http://www.codeguru.com/cpp/data/mfc_database/ado/article.php/c6729/Using-ADO-from-C.htm

using namespace std;
using namespace utility;

class Database
{
private:
  static string connectionString;
  Database() { ::CoInitialize(NULL); };

public:
  Database(Database const&) = delete;
  void operator=(Database const&) = delete;
  ~Database();
  static Database& GetInstance();
  static void SetConnectionString(string connection_string);
  _ConnectionPtr GetConnection();
  _RecordsetPtr Execute(string cmd);
  map<string, string> FindTrader(string username);
  map<string, string> RegisterTrader(string username, string password);
  vector<map<string, string>> Transactions(string username);
  vector<map<string, string>> Portfolio(string username);
  map<string, string> Portfolio(string username, string stockcode);
  map<string, double> Quote(string stockcode);
  map<string, double> OwnedStock(string username, string stockcode);
  double StockPrice(string stockcode);
  double TraderBalance(string username);
  bool AdjustBalance(string username, double delta);
  bool InsertTransaction(string username, string stockcode, double quantity, string status, string type);
  bool UpdatePortfolio(string username, string stockcode, double quantity, double cost);
  bool InsertPortfolio(string username, string stockcode, double quantity, double cost);
  vector<map<string, string>> QuoteList();
  bool DeleteTrader(string username);
};