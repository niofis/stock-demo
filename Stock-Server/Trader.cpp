#include "stdafx.h"
#include "Trader.h"

using namespace std;

Trader::Trader(string username, string password)
{
  this->username = username;
  this->password = password;
  this->isLoggedIn = false;
}

map<string, string> Trader::Register()
{
  map<string, string> result;
  auto dup = Database::GetInstance().FindTrader(username);
  
  if (dup["username"] == username) {
    result["error"] = "trader already registered.";
    return result;
  }

  result = Database::GetInstance().RegisterTrader(username, password);

  return result;
}

bool Trader::Login()
{
  if (username.size() == 0 || password.size() == 0)
    return false;

  auto user = Database::GetInstance().FindTrader(utility::conversions::to_utf8string(username));
  auto pwd = utility::conversions::to_utf8string(password);

  //TODO add password hashing

  if (user["password"] == pwd) {
    isLoggedIn = true;
    return true;
  }
  return false;
}

bool Trader::Delete()
{
  return Database::GetInstance().DeleteTrader(username);
}

vector<Transaction> Trader::GetTransactions()
{
  vector<Transaction> transactions;
  auto data = Database::GetInstance().Transactions(username);
  for(auto it = data.begin(); it != data.end(); it++) {
    Transaction tr(*it);
    transactions.push_back(tr);
  }
  return transactions;
}

vector<Portfolio> Trader::GetPortfolio()
{
  vector<Portfolio> portfolio;
  auto data = Database::GetInstance().Portfolio(username);
  for (auto it = data.begin(); it != data.end(); it++) {
    Portfolio tr(*it);
    portfolio.push_back(tr);
  }
  return portfolio;
}

Portfolio Trader::GetPortfolio(string stockcode)
{
  Portfolio stock(Database::GetInstance().Portfolio(username, stockcode));
  return stock;
}

double Trader::GetBalance()
{
  return Database::GetInstance().TraderBalance(username);
}
