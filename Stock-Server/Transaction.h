#pragma once
#pragma once
#include "stdafx.h"
#include "Database.h"
#include "Entity.h"

using namespace std;
using namespace web;

class Transaction : public Entity
{
public:
  string id;
  string username;
  string stockcode;
  float quantity;
  string datetime;
  string status;
  string type;

  Transaction();
  Transaction(map<string, string> data);
  bool Create();
  json::value ToJSON();
};