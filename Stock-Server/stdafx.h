// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"


#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

#include "cpprest\json.h"
#include "cpprest\http_listener.h"
#include "cpprest\uri.h"
#include "cpprest\asyncrt_utils.h"


// TODO: reference additional headers your program requires here
