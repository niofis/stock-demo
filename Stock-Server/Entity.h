#pragma once
#include "stdafx.h"

using namespace web;

class Entity
{
public:
  virtual json::value ToJSON() = 0;
};