// Stock-Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Database.h"
#include "Controller.h"
#include "StockService.h"
#include "TraderService.h"



using namespace std;
using namespace utility;
using namespace web;
using namespace http;
using namespace http::experimental::listener;

unique_ptr<Controller> g_httpStockService;
unique_ptr<Controller> g_httpTraderService;

void initialize(const string_t& address)
{

  /*Load connection string from file*/
  fstream fs;
  fs.open("connection_string.txt", fstream::in);
  string cs;
  getline(fs, cs);
  fs.close();
  Database::SetConnectionString(cs);

  /*Configure controllers*/
  uri_builder stock_uri(address);
  stock_uri.append_path(U("stock"));
  auto stock_addr = stock_uri.to_uri().to_string();
  g_httpStockService = unique_ptr<StockService>(new StockService(stock_addr));
  g_httpStockService->open().wait();

  uri_builder trader_uri(address);
  trader_uri.append_path(U("trader"));
  auto trader_addr = trader_uri.to_uri().to_string();
  g_httpTraderService = unique_ptr<TraderService>(new TraderService(trader_addr));
  g_httpTraderService->open().wait();

  ucout << utility::string_t(U("Listening for requests at:")) << address << endl;
}

void shutdown() {
  g_httpStockService->close().wait();
  g_httpTraderService->close().wait();
}

int main(int argc, char *argv[])
{
  ::CoInitialize(NULL);
  string_t port = U("8080");

  for (int i = 1; i < argc; i++) {
    string arg(argv[i]);

    if (arg.compare("-p")) {
      arg = argv[i + 1];
      port = conversions::to_utf16string(arg);
    }
  }

  string_t address = U("http://127.0.0.1:");
  address.append(port);

  initialize(address);

  cout << "Press ENTER to exit." << endl;
  string line;
  getline(cin, line);

  shutdown();

  ::CoUninitialize();
  
  return 0;
}

