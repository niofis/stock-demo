#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;

namespace Tests
{		

  string_t test_user = U("test_user");
  string_t test_password = U("1q2w3e");

  json::value plain_request(string_t path, json::value data) {
    string_t base_url = U("http://127.0.0.1:8080");
    base_url += path;
    http_request request;
    request.set_body(data);
    request.set_method(methods::POST);
    request.headers().set_content_type(U("application/json"));
  
    uri _uri(base_url);
    http_client client(_uri);
    http_response response = client.request(request).get();
    return response.extract_json().get();
  }

  json::value auth_request(string_t path, json::value data) {
    data[U("username")] = json::value::string(test_user);
    data[U("password")] = json::value::string(test_password);
    return plain_request(path, data);
  }

	TEST_CLASS(UnitTest)
	{
	public:
    TEST_METHOD(RegistrationTest)
    {
      json::value res;

      json::value _empty_data;
      res = plain_request(U("/trader/register"), _empty_data);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"Empty credentials must return error");


      json::value _invald_credentials_1;
      _invald_credentials_1[U("username")] = json::value::string(U("alguien"));
      res = plain_request(U("/trader/register"), _invald_credentials_1);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"No password must return error");
      
      json::value _invald_credentials_2;
      _invald_credentials_2[U("password")] = json::value::string(U("123456"));
      res = plain_request(U("/trader/register"), _invald_credentials_2);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"No username must return error");
    
      json::value _valid_credentials_3;
      _valid_credentials_3[U("username")] = json::value::string(test_user);
      _valid_credentials_3[U("password")] = json::value::string(test_password);
      res = plain_request(U("/trader/register"), _valid_credentials_3);
      Assert::AreEqual(U("ok"), res[U("result"]).as_string().c_str(), L"User must have registered correctly.");

      res = plain_request(U("/trader/register"), _valid_credentials_3);
      Assert::AreEqual(U("trader already registered."), res[U("error"]).as_string().c_str(), L"Cannot register duplicated user.");

    }

		TEST_METHOD(LoginTest)
		{
      json::value res;
      
      //auto res = request(U("/trader/login"), data);
      //Assert::AreEqual(res[U("result"]).as_string().c_str(), U("ok"), L"Correct credentials must login");
      json::value _empty_data;
      res = plain_request(U("/trader/login"), _empty_data);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"Empty credentials must return error");

      json::value _invald_credentials_1;
      _invald_credentials_1[U("username")] = json::value::string(test_user);
      _invald_credentials_1[U("password")] = json::value::string(U(""));
      res = plain_request(U("/trader/login"), _invald_credentials_1);
      Assert::AreEqual(U("invalid credentials."), res[U("error"]).as_string().c_str(), L"Empty password must return error");

      json::value _invald_credentials_2;
      _invald_credentials_2[U("username")] = json::value::string(U(""));
      _invald_credentials_2[U("password")] = json::value::string(test_password);
      res = plain_request(U("/trader/login"), _invald_credentials_2);
      Assert::AreEqual(U("invalid credentials."), res[U("error"]).as_string().c_str(), L"Empty username must return error");

      json::value _invald_credentials_3;
      _invald_credentials_3[U("username")] = json::value::string(test_user);
      _invald_credentials_3[U("password")] = json::value::string(U("1234561"));
      res = plain_request(U("/trader/login"), _invald_credentials_3);
      Assert::AreEqual(U("invalid credentials."), res[U("error"]).as_string().c_str(), L"Incorrect password must return error");
      
      json::value _invald_credentials_4;
      _invald_credentials_4[U("username")] = json::value::string(U("alguien1"));
      _invald_credentials_4[U("password")] = json::value::string(test_password);
      res = plain_request(U("/trader/login"), _invald_credentials_4);
      Assert::AreEqual(U("invalid credentials."), res[U("error"]).as_string().c_str(), L"Incorrect username must return error");
		}

    TEST_METHOD(TradingOperationsTest)
    {
      json::value res;

      json::value _test_data1;
      res = auth_request(U("/stock/list"), _test_data1);
      Assert::AreEqual(true, res.is_array(), L"Must return an array of available stocks.");
      auto arr = res.as_array();
      auto stock = arr[0];

      Assert::IsTrue(stock[U("lastsaleprice")].is_number(), L"Returned stock must contain lastsaleprice");
      Assert::IsTrue(stock[U("stockcode")].is_string(), L"Returned stock must contain stockcode");

      json::value _test_data2;
      _test_data2[U("stockcode")] = stock[U("stockcode")];
      res = auth_request(U("/stock/quote"), _test_data2);
      Assert::IsTrue(res[U("lastsaleprice")].is_number(), L"Quote must have lastsaleprice.");

      json::value _test_data3;
      res = auth_request(U("/trader/balance"), _test_data3);
      Assert::IsTrue(res[U("balance")].is_number(), L"Balance must return a number.");
      Assert::AreEqual(100000.0, res[U("balance")].as_double(), L"Initial balance must be 100,000.");

      res = auth_request(U("/trader/portfolio"), _test_data3);
      Assert::IsTrue(res.is_array(), L"Porfolio must be an array.");
      Assert::IsTrue(res.as_array().size() == 0, L"Inital portfolio must be empty.");

      res = auth_request(U("/trader/transactions"), _test_data3);
      Assert::IsTrue(res.is_array(), L"Transactions log must be an array.");
      Assert::IsTrue(res.as_array().size() == 0, L"Inital transactions log must be empty.");

      json::value _test_data4;
      _test_data4[U("stockcode")] = stock[U("stockcode")];
      _test_data4[U("quantity")] = json::value::number(1000000.0);
      _test_data4[U("price")] = json::value::number(1000000.0);
      res = auth_request(U("/stock/buy"), _test_data4);
      Assert::AreEqual(U("don't have enough money."), res[U("error")].as_string().c_str(), L"Must not let you buy if you don't have enough money.");
    
      json::value _test_data5;
      _test_data5[U("stockcode")] = stock[U("stockcode")];
      _test_data5[U("quantity")] = json::value::number(1000000.0);
      _test_data5[U("price")] = json::value::number(1000000.0);
      res = auth_request(U("/stock/sell"), _test_data5);
      Assert::AreEqual(U("can't sell more stock than owned."), res[U("error")].as_string().c_str(), L"Must not let you sell stock you don't have.");

      json::value _test_data6;
      _test_data6[U("stockcode")] = stock[U("stockcode")];
      _test_data6[U("quantity")] = json::value::number(1.0);
      _test_data6[U("price")] = json::value::number(100.0);
      res = auth_request(U("/stock/buy"), _test_data6);
      Assert::AreEqual(U("ok"), res[U("result")].as_string().c_str(), L"Should be allowed to buy stock.");

      json::value _test_data7;
      res = auth_request(U("/trader/portfolio"), _test_data7);
      Assert::IsTrue(res.as_array().size() == 1, L"Current portfolio must have one item.");
      auto item = res.as_array()[0];
      Assert::AreEqual(test_user, item[U("username")].as_string(), L"Portfolio username must match.");
      Assert::AreEqual(stock[U("stockcode")].as_string(), item[U("stockcode")].as_string(), L"Portfolio stockcode must match.");
      Assert::AreEqual(1.0, item[U("quantity")].as_double(), L"Portfolio quantity must match.");
      Assert::AreEqual(stock[U("lastsaleprice")].as_double(), item[U("totalcost")].as_double(), L"Portfolio totalcost must match.");

      json::value _test_data8;
      res = auth_request(U("/trader/transactions"), _test_data8);
      Assert::IsTrue(res.as_array().size() == 1, L"Current transactions log must have one item.");
      auto item2 = res.as_array()[0];
      Assert::AreEqual(test_user, item2[U("username")].as_string(), L"Transaction username must match.");
      Assert::AreEqual(stock[U("stockcode")].as_string(), item2[U("stockcode")].as_string(), L"Transaction stockcode must match.");
      Assert::AreEqual(1.0, item2[U("quantity")].as_double(), L"Transaction quantity must match.");
      Assert::AreEqual(U("EXECUTED"), item2[U("status")].as_string().c_str(), L"Transaction status must be 'EXECUTED'.");
      Assert::AreEqual(U("BUY"), item2[U("type")].as_string().c_str(), L"Transaction type must be 'BUY'.");
      Assert::IsTrue(item2[U("datetime")].is_string(), L"Transaction must contain datetime.");

      json::value _test_data9;
      res = auth_request(U("/trader/balance"), _test_data9);
      Assert::AreEqual(99900.0, res[U("balance")].as_double(), L"Initial balance must be 99,900.");
    
      
      json::value _test_data10;
      _test_data10[U("stockcode")] = stock[U("stockcode")];
      _test_data10[U("quantity")] = json::value::number(1.0);
      _test_data10[U("price")] = json::value::number(100.0);
      res = auth_request(U("/stock/sell"), _test_data10);
      Assert::AreEqual(U("ok"), res[U("result")].as_string().c_str(), L"Must be able to sell owned stock.");

      json::value _test_data11;
      res = auth_request(U("/trader/portfolio"), _test_data11);
      auto item3 = res.as_array()[0];
      Assert::AreEqual(0.0, item3[U("quantity")].as_double(), L"Owned stock must be zero after sale.");

      json::value _test_data12;
      res = auth_request(U("/trader/transactions"), _test_data12);
      Assert::IsTrue(res.as_array().size() == 2, L"Current transactions log must have two items.");
      auto item4 = res.as_array()[0];
      Assert::AreEqual(stock[U("stockcode")].as_string(), item4[U("stockcode")].as_string(), L"Transaction stockcode must match.");
      Assert::AreEqual(1.0, item4[U("quantity")].as_double(), L"Transaction quantity must match.");
      Assert::AreEqual(U("EXECUTED"), item4[U("status")].as_string().c_str(), L"Transaction status must be 'EXECUTED'.");
      Assert::AreEqual(U("SELL"), item4[U("type")].as_string().c_str(), L"Transaction type must be 'SELL'.");
      Assert::IsTrue(item4[U("datetime")].is_string(), L"Transaction must contain datetime.");
      
    }

    TEST_METHOD(TraderDeleteTest)
    {
      json::value res;

      json::value _invald_credentials_1;
      _invald_credentials_1[U("username")] = json::value::string(test_user);
      res = plain_request(U("/trader/delete"), _invald_credentials_1);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"No password must return error");

      json::value _invald_credentials_2;
      _invald_credentials_2[U("password")] = json::value::string(U("123456"));
      res = plain_request(U("/trader/delete"), _invald_credentials_2);
      Assert::AreEqual(U("incomplete credentials."), res[U("error"]).as_string().c_str(), L"No username must return error");
      
      json::value _valid_credentials_3;
      _valid_credentials_3[U("username")] = json::value::string(test_user);
      _valid_credentials_3[U("password")] = json::value::string(test_password);
      res = plain_request(U("/trader/delete"), _valid_credentials_3);
      Assert::AreEqual(U("ok"), res[U("result"]).as_string().c_str(), L"User must have been deleted correctly");
    }

	};
}