// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include "cpprest\json.h"
#include "cpprest\http_client.h"
#include "cpprest\uri.h"
#include "cpprest\asyncrt_utils.h"


// Headers for CppUnitTest
#include "CppUnitTest.h"

// TODO: reference additional headers your program requires here
