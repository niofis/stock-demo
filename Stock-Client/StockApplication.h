#pragma once
#include "stdafx.h"
#include "User.h"

using namespace Wt;

class StockApplication : public WApplication
{
public:
  StockApplication(const WEnvironment& env);
  ~StockApplication();
  User *currentUser;
  void Reload();

private:
  WLineEdit *nameEdit_;
  WText *greeting_;

  WNavigationBar *navigation_;
  WStackedWidget *contentsStack_;

  
};