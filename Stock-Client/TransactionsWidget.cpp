#include "stdafx.h"
#include "TransactionsWidget.h"
#include "Request.h"

TransactionsWidget::TransactionsWidget()
{
  auto container = this;
  container->addWidget(new WText("Transactions Log"));

  Wt::WTable *table = new Wt::WTable();
  table->setHeaderCount(1);
  table->setWidth(WLength("100%"));
  table->addStyleClass("table table-striped");
  table->elementAt(0, 0)->addWidget(new Wt::WText("Code"));
  table->elementAt(0, 1)->addWidget(new Wt::WText("Quantity"));
  table->elementAt(0, 2)->addWidget(new Wt::WText("Date/Time"));
  table->elementAt(0, 3)->addWidget(new Wt::WText("Type"));
  table->elementAt(0, 4)->addWidget(new Wt::WText("Status"));


  Request request;
  auto data = request.GetTransactions();

  if (data.is_array() == true) {
    for (int i = 0; i < data.size(); i++) {
      auto val = data[i];
      string code = conversions::to_utf8string(val[U("stockcode")].as_string());
      string quantity = conversions::to_utf8string(val[U("quantity")].serialize());
      string datetime = conversions::to_utf8string(val[U("datetime")].as_string());
      string type = conversions::to_utf8string(val[U("type")].as_string());
      string status = conversions::to_utf8string(val[U("status")].as_string());
      table->elementAt(i + 1, 0)->addWidget(new Wt::WText(code));
      table->elementAt(i + 1, 1)->addWidget(new Wt::WText(quantity));
      table->elementAt(i + 1, 2)->addWidget(new Wt::WText(datetime));
      table->elementAt(i + 1, 3)->addWidget(new Wt::WText(type));
      table->elementAt(i + 1, 4)->addWidget(new Wt::WText(status));
    }
  }



  container->addWidget(table);
}
