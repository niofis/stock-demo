#include "stdafx.h"
#include "PortfolioWidget.h"
#include "SellStockDialog.h"
#include "Request.h"

using namespace std;
using namespace Wt;
using namespace web;

class SellButton : public WPushButton
{
public:
  SellButton(string stockcode);
};

SellButton::SellButton(string stockcode)
  :WPushButton("Sell")
{
  this->clicked().connect(std::bind([=]() {
    SellStockDialog *dialog = new SellStockDialog(stockcode);

    dialog->finished().connect(std::bind([=]() {
      delete dialog;
    }));

    dialog->show();
  }));

}

PortfolioWidget::PortfolioWidget() : WContainerWidget()
{
  auto container = this;
  container->addWidget(new WText("Owned Stock"));

  Wt::WTable *table = new Wt::WTable();
  table->setHeaderCount(1);
  table->setWidth(WLength("100%"));
  table->addStyleClass("table table-striped");
  table->elementAt(0, 0)->addWidget(new Wt::WText("Code"));
  table->elementAt(0, 1)->addWidget(new Wt::WText("Quantity"));
  table->elementAt(0, 2)->addWidget(new Wt::WText("Total Cost"));
  table->elementAt(0, 3)->addWidget(new Wt::WText(""));


  Request request;
  auto data = request.GetPortfolio();

  if (data.is_array() == true) {
    for (int i = 0; i < data.size(); i++) {
      auto val = data[i];
      string code = conversions::to_utf8string(val[U("stockcode")].as_string());
      string quantity = conversions::to_utf8string(val[U("quantity")].serialize());
      string cost = conversions::to_utf8string(val[U("totalcost")].serialize());
      table->elementAt(i + 1, 0)->addWidget(new Wt::WText(code));
      table->elementAt(i + 1, 1)->addWidget(new Wt::WText(quantity));
      table->elementAt(i + 1, 2)->addWidget(new Wt::WText(cost));
      table->elementAt(i + 1, 3)->addWidget(new SellButton(code));
    }
  }

  container->addWidget(table);
}
