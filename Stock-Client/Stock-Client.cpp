// Stock-Client.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Request.h"
#include "StockApplication.h"

//Based on examples from:
//https://www.webtoolkit.eu/wt/examples/


WApplication *createApplication(const WEnvironment& env)
{
  /*Load connection string from file*/
  fstream fs;
  fs.open("api_url.txt", fstream::in);
  string cs;
  getline(fs, cs);
  fs.close();
  Request::SetAPIBaseURL(conversions::to_string_t(cs));



  return new StockApplication(env);
}


int main(int argc, char **argv)
{
  return WRun(argc, argv, &createApplication);
}

