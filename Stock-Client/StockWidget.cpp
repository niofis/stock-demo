#include "stdafx.h"
#include "StockWidget.h"
#include "BuyStockDialog.h"
#include "Request.h"

using namespace Wt;
using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;

class BuyButton : public WPushButton
{
public:
  BuyButton(string stockcode, string price);
};

BuyButton::BuyButton(string stockcode, string price)
  :WPushButton("Buy")
{
  this->clicked().connect(std::bind([=]() {
    BuyStockDialog *dialog = new BuyStockDialog(stockcode, price);

    dialog->finished().connect(std::bind([=]() {
      delete dialog;
    }));

    dialog->show();
  }));

}

WTable* StockWidget::GetTable()
{
  WTable *table = new WTable();
  table->setHeaderCount(1);
  table->setWidth(WLength("100%"));
  table->addStyleClass("table table-striped");
  table->elementAt(0, 0)->addWidget(new Wt::WText("Code"));
  table->elementAt(0, 1)->addWidget(new Wt::WText("Sale Price"));
  table->elementAt(0, 2)->addWidget(new Wt::WText(""));

  Request request;
  auto data = request.GetStockList();

  if (data.is_array() == true) {
    for (int i = 0; i < data.size(); i++) {
      auto val = data[i];
      string code = conversions::to_utf8string(val[U("stockcode")].as_string());
      string price = conversions::to_utf8string(val[U("lastsaleprice")].serialize());
      table->elementAt(i + 1, 0)->addWidget(new Wt::WText(code));
      table->elementAt(i + 1, 1)->addWidget(new Wt::WText(price));
      table->elementAt(i + 1, 2)->addWidget(new BuyButton(code, price));
    }
  }
  

  return table;
}

StockWidget::StockWidget()
{
  auto container = this;
  container->addWidget(new WText("Available Stock"));
  container->addWidget(GetTable());
}

