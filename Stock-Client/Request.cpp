#include "stdafx.h"
#include "StockApplication.h"
#include "Request.h"

string_t Request::_base_url = U("");

void Request::PrepareRequest()
{
  StockApplication *app = (StockApplication*)WApplication::instance();

  if (app->currentUser != NULL) {
    _request_data[U("username")] = json::value::string(conversions::to_string_t(app->currentUser->username));
    _request_data[U("password")] = json::value::string(conversions::to_string_t(app->currentUser->password));
  }

  _request.set_body(_request_data);
  _request.set_method(methods::POST);
  _request.headers().set_content_type(U("application/json"));
}

Request::Request()
{
  PrepareRequest();
}

Request::~Request()
{
}

void Request::SetAPIBaseURL(string_t url)
{
  _base_url = url;
}

json::value Request::GetStockList()
{
  return GetResponse(U("/stock/list"));
}

json::value Request::SellStock(string stockcode, string qty, string price)
{
  _request_data[U("stockcode")] = json::value::string(conversions::to_string_t(stockcode));
  _request_data[U("quantity")] = json::value::number(stod(qty));
  _request_data[U("price")] = json::value::number(stod(price));

  return GetResponse(U("/stock/sell"));
}

json::value Request::BuyStock(string stockcode, string qty, string price)
{
  _request_data[U("stockcode")] = json::value::string(conversions::to_string_t(stockcode));
  _request_data[U("quantity")] = json::value::number(stod(qty));
  _request_data[U("price")] = json::value::number(stod(price));

  return GetResponse(U("/stock/buy"));
}

json::value Request::GetTransactions()
{
  return GetResponse(U("/trader/transactions"));
}

json::value Request::GetPortfolio()
{
  return GetResponse(U("/trader/portfolio"));
}

json::value Request::GetBalance()
{
  return GetResponse(U("/trader/balance"));
}

json::value Request::GetQuote(string stockcode)
{
  _request_data[U("stockcode")] = json::value::string(conversions::to_string_t(stockcode));

  return GetResponse(U("/stock/quote"));
}

json::value Request::Login(string username, string password)
{
  _request_data[U("username")] = json::value::string(conversions::to_string_t(username));
  _request_data[U("password")] = json::value::string(conversions::to_string_t(password));

  return GetResponse(U("/trader/login"));
}

json::value Request::Register(string username, string password)
{
  _request_data[U("username")] = json::value::string(conversions::to_string_t(username));
  _request_data[U("password")] = json::value::string(conversions::to_string_t(password));

  return GetResponse(U("/trader/register"));
}

json::value Request::GetResponse(string_t path)
{
  uri _uri(_base_url + path);

  _request.set_body(_request_data);

  http_client client(_uri);
  http_response response = client.request(_request).get();

  return response.extract_json().get();
}