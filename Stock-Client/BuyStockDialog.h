#pragma once

#include "stdafx.h"

using namespace std;
using namespace Wt;

class BuyStockDialog : public WDialog
{
public:
  BuyStockDialog(string stockcode, string price);
};