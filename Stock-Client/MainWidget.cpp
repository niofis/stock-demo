#include "stdafx.h"
#include "StockApplication.h"
#include "MainWidget.h"
#include "PortfolioWidget.h"
#include "TransactionsWidget.h"
#include "StockWidget.h"
#include "Request.h"

using namespace Wt;

MainWidget::MainWidget()
  : WContainerWidget()
{
  setOverflow(OverflowHidden);

  navigation_ = new Wt::WNavigationBar();
  navigation_->addStyleClass("main-nav");
  navigation_->setTitle("Stock");
  navigation_->setResponsive(true);

  contentsStack_ = new Wt::WStackedWidget();

  Wt::WAnimation animation(Wt::WAnimation::Fade,
    Wt::WAnimation::Linear,
    200);
  contentsStack_->setTransitionAnimation(animation, true);

  Wt::WMenu *menu = new Wt::WMenu(contentsStack_, 0);
  //menu->setInternalPathEnabled();
  //menu->setInternalBasePath("/");

  addToMenu(menu, "Portfolio", new PortfolioWidget());
  addToMenu(menu, "Transactions", new TransactionsWidget());
  addToMenu(menu, "Stock", new StockWidget());

  Request req;
  json::value res = req.GetBalance();

  if (res.is_null() == false && res[U("balance")].is_number()) {
    string str = "Current Balance: ";
    std::ostringstream strs;
    strs << res[U("balance")].as_number().to_double();
    str += strs.str();
    menu->addItem(str);
  }

  WMenuItem *logoutItem = new WMenuItem("Logout");
  logoutItem->clicked().connect(std::bind([=]() {
    auto app = (StockApplication *)WApplication::instance();
    //clear cookies
    app->removeCookie("username");
    app->removeCookie("password");
    app->Reload();
  }));

  menu->addItem(logoutItem);

  navigation_->addMenu(menu);

  Wt::WVBoxLayout *layout = new Wt::WVBoxLayout(this);
  layout->addWidget(navigation_);
  layout->addWidget(contentsStack_, 1);
  layout->setContentsMargins(0, 0, 0, 0);
}

Wt::WMenuItem * MainWidget::addToMenu(Wt::WMenu * menu, const Wt::WString & name, WContainerWidget * widget)
{
  Wt::WContainerWidget *result = new Wt::WContainerWidget();

  Wt::WContainerWidget *pane = new Wt::WContainerWidget();

  Wt::WVBoxLayout *vLayout = new Wt::WVBoxLayout(result);
  vLayout->setContentsMargins(0, 0, 0, 0);
  vLayout->addWidget(widget);
  vLayout->addWidget(pane, 1);

  Wt::WHBoxLayout *hLayout = new Wt::WHBoxLayout(pane);

  Wt::WMenuItem *item = new Wt::WMenuItem(name, result);
  menu->addItem(item);

  Wt::WStackedWidget *subStack = new Wt::WStackedWidget();
  subStack->addStyleClass("contents");
  subStack->setOverflow(WContainerWidget::OverflowAuto);

  return item;
}