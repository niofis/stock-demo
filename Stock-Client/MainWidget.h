#pragma once
#include "stdafx.h"
using namespace Wt;

class MainWidget : public WContainerWidget
{
public:
  MainWidget();
  Wt::WNavigationBar *navigation_;
  Wt::WStackedWidget *contentsStack_;

  Wt::WMenuItem *addToMenu(Wt::WMenu *menu,
    const Wt::WString& name,
    WContainerWidget *widget);
};
