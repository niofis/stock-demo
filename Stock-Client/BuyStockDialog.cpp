#include "stdafx.h"
#include "StockApplication.h"
#include "BuyStockDialog.h"
#include "Request.h"

using namespace std;
using namespace Wt;

BuyStockDialog::BuyStockDialog(string stockcode, string price) : WDialog("Buy Stock")
{
  auto dialog = this;

  dialog->contents()->addStyleClass("form-group");

  WLabel *qtyLabel = new WLabel("Quantity", dialog->contents());
  WLineEdit *qtyEdit = new WLineEdit(dialog->contents());
  qtyLabel->setBuddy(qtyEdit);

  WRegExpValidator *numValidator = new WRegExpValidator("[0-9]+");
  numValidator->setMandatory(true);
  qtyEdit->setValidator(numValidator);
  qtyEdit->setText("1");

  WLabel *prcLabel = new WLabel("Price", dialog->contents());
  WLineEdit *prcEdit = new WLineEdit(dialog->contents());
  prcLabel->setBuddy(prcEdit);

  WRegExpValidator *floatValidator = new WRegExpValidator("[0-9]+(\.[0-9]+)?");
  floatValidator->setMandatory(true);
  prcEdit->setValidator(floatValidator);
  prcEdit->setText(price);

  WPushButton *ok = new WPushButton("OK", dialog->footer());
  ok->setDefault(true);

  Wt::WPushButton *cancel = new Wt::WPushButton("Cancel", dialog->footer());
  dialog->rejectWhenEscapePressed();

  qtyEdit->keyWentUp().connect(std::bind([=]() {
    ok->setDisabled(qtyEdit->validate() != Wt::WValidator::Valid);
  }));

  prcEdit->keyWentUp().connect(std::bind([=]() {
    ok->setDisabled(prcEdit->validate() != Wt::WValidator::Valid);
  }));

  ok->clicked().connect(std::bind([=]() {
    if (qtyEdit->validate()) {
      Request req;
      json::value res = req.BuyStock(stockcode, qtyEdit->text().toUTF8(), prcEdit->text().toUTF8());
      
      if (res.is_null() || res[U("error")].is_string()) {
        string error = conversions::to_utf8string(res[U("error")].as_string());
          WMessageBox::show("Error", error, Wt::Ok);
        dialog->reject();
      }
      else {
        WMessageBox::show("Success!", "Buy was successful!", Wt::Ok);
        dialog->accept();

        auto app = (StockApplication *)WApplication::instance();
        app->Reload();
      }
    }
  }));

  cancel->clicked().connect(dialog, &Wt::WDialog::reject);
}
