#include "LoginWidget.h"
#include "stdafx.h"
#include "RegisterWidget.h"
#include "LoginWidget.h"
#include "Request.h"

using namespace std;
using namespace Wt;
using namespace web;

RegisterWidget::RegisterWidget() : WContainerWidget()
{
  WContainerWidget *container = new WContainerWidget();

  WPanel *panel = new WPanel();
  panel->setWidth(Wt::WLength("400px"));
  panel->setCentralWidget(container);

  this->addWidget(panel);

  container->addWidget(new WText("Username"));

  auto usernameEdit = new WLineEdit(container);
  usernameEdit->setFocus();

  container->addWidget(new WText("Password"));
  auto passwordEdit = new WLineEdit(container);
  passwordEdit->setEchoMode(WLineEdit::EchoMode::Password);

  container->addWidget(new WBreak());

  WPushButton *registerButton
    = new WPushButton("Register", container);
  registerButton->setStyleClass("btn-primary btn-block");


  registerButton->clicked().connect(std::bind([=]() {
    Request req;
    string username = usernameEdit->text().toUTF8();
    string password = passwordEdit->text().toUTF8();
    json::value res = req.Register(username, password);

    if (res.is_null() || res[U("error")].is_string()) {
      string error = conversions::to_utf8string(res[U("error")].as_string());
      WMessageBox::show("Error", error, Wt::Ok);
    }
    else {
      auto container = (WContainerWidget *)this->parent();
      container->clear();
      container->addWidget(new LoginWidget());
    }
  }));
}
