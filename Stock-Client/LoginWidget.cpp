#include "stdafx.h"
#include "StockApplication.h"
#include "MainWidget.h"
#include "LoginWidget.h"
#include "RegisterWidget.h"
#include "Request.h"

using namespace Wt;

LoginWidget::LoginWidget() : WContainerWidget()
{

  WContainerWidget *container = new WContainerWidget();

  WPanel *panel = new WPanel();
  panel->setWidth(Wt::WLength("400px"));
  panel->setCentralWidget(container);

  this->addWidget(panel);

  container->addWidget(new WText("Username"));

  auto usernameEdit = new WLineEdit(container);
  usernameEdit->setFocus();

  container->addWidget(new WText("Password"));
  auto passwordEdit = new WLineEdit(container);
  passwordEdit->setEchoMode(WLineEdit::EchoMode::Password);

  container->addWidget(new WBreak());

  WPushButton *loginButton
    = new WPushButton("Login", container);
  loginButton->setMargin(5, Left);
  loginButton->setStyleClass("btn-primary btn-block");

  container->addWidget(new WBreak());

  WPushButton *registerButton
    = new WPushButton("Register", container);
  registerButton->setStyleClass("btn-info btn-block");

  

  loginButton->clicked().connect(std::bind([=]() {

    Request req;
    string username = usernameEdit->text().toUTF8();
    string password = passwordEdit->text().toUTF8();
    json::value res = req.Login(username, password);

    if (res.is_null() || res[U("error")].is_string()) {
      string error = conversions::to_utf8string(res[U("error")].as_string());
      WMessageBox::show("Error", error, Wt::Ok);
    }
    else {
      auto app = (StockApplication *) WApplication::instance();
      app->currentUser = new User(username, password);
      auto container = (WContainerWidget *)this->parent();
      container->clear();
      container->addWidget(new MainWidget());
      //this cookie will last 24 active
      app->setCookie("username", username, 24 * 60 * 1000);
      app->setCookie("password", password, 24 * 60 * 1000);
    }
  }));

  registerButton->clicked().connect(std::bind([=]() {
    auto container = (WContainerWidget *)this->parent();
    container->clear();
    container->addWidget(new RegisterWidget());
  }));
}
