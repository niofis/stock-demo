// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <fstream>
#include "cpprest\json.h"
#include "cpprest\http_client.h"
#include "cpprest\uri.h"
#include "cpprest\asyncrt_utils.h"

#include <Wt/WApplication>
#include <Wt/WBreak>
#include <Wt/WContainerWidget>
#include <Wt/WLineEdit>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WBootstrapTheme>
#include <Wt/WMenu>
#include <Wt/WStackedWidget>
#include <Wt/WTextArea>
#include <Wt/WCssTheme>
#include <Wt/WHBoxLayout>
#include <Wt/WBorderLayout>
#include <Wt/WNavigationBar>
#include <Wt/WVBoxLayout>
#include <Wt/WTable>
#include <Wt/WTableCell>
#include <Wt/WDialog>
#include <Wt/WLabel>
#include <Wt/WRegExpValidator>
#include <Wt/WMessageBox>
#include <Wt/WPanel>
#include <Wt/WEnvironment>



// TODO: reference additional headers your program requires here
