#include "stdafx.h"
#include "StockApplication.h"
#include "MainWidget.h"
#include "LoginWidget.h"

using namespace Wt;


StockApplication::StockApplication(const WEnvironment& env)
  : WApplication(env)
{
  auto app = this;
  
  WBootstrapTheme *bootstrapTheme = new WBootstrapTheme(app);
  bootstrapTheme->setVersion(WBootstrapTheme::Version3);
  bootstrapTheme->setResponsive(true);
  app->setTheme(bootstrapTheme);

  app->useStyleSheet("resources/themes/bootstrap/3/bootstrap-theme.min.css");

  setTitle("Stock App");
  
  const string *username = env.getCookieValue("username");
  const string *password = env.getCookieValue("password");
  if (username && password) {
    app->currentUser = new User(*username, *password);
    root()->addWidget(new MainWidget());
  }
  else
  {
    root()->addWidget(new LoginWidget());
  }
  
}

StockApplication::~StockApplication()
{
  if (currentUser != NULL)
    delete currentUser;
}

void StockApplication::Reload()
{
  auto app = this;
  string url = app->bookmarkUrl();
  app->redirect(url);
}
