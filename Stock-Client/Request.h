#pragma once
#include "stdafx.h"

using namespace std;
using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;



class Request {
private:
  json::value _body;
  http_request _request;
  json::value _request_data;

  void PrepareRequest();
  json::value GetResponse(string_t path);

  static string_t _base_url;

public:
  Request();
  ~Request();

  static void SetAPIBaseURL(string_t url);

  json::value GetStockList();
  json::value SellStock(string stockcode, string qty, string price);
  json::value BuyStock(string stockcode, string qty, string price);
  json::value GetTransactions();
  json::value GetPortfolio();
  json::value GetBalance();
  json::value GetQuote(string stockcode);
  json::value Login(string username, string password);
  json::value Register(string username, string password);
};